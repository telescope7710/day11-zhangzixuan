## O
- We reviewed basic HTML concepts such as form elements, replaced elements, semantic elements.
- A ! delimiter followed by the important keyword marks the declaration as important. This statement will override any other statements.
- We use JSX to describe React component.
- We used a multi-counter project to practice using hook and passing passing variables between components.
## R
- Passing variable in react component is so complex that I've been in turmoil all afternoon.
- I'm not familiar with JSX.
## I
- The standardized use of semantic elements is very important, not only for search engines to determine the context and the weight of individual keywords based on the tags, but also for blind readers to render web pages based on semantic elements.
- It's best to use an external CSS file in order to mange our projects easier.
## D
- I need to learn more about JSX.