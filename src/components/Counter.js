import { useState } from "react";

const Counter = (props) => {
    const {counter,updateCounterValue,index} = props;
    const increase = (e) =>{
        updateCounterValue(counter+1,index);
    }
    const decrease = (e) =>{
        updateCounterValue(counter-1,index);
    }
    return(
        <div className='counter'>
            <button onClick={increase}>+</button>
            <h3>{counter}</h3>
            <button onClick={decrease}>-</button>
        </div> );
};
export default Counter;