import Counter from "./Counter";
const CounterGroup = (props) =>{
    const {counterList,setCounterList} = props;
    const updateCounterValue = (newCounterValue,index)=>{
        const list = counterList.map((currentValue,itemIndex)=>{
            if(itemIndex===index){
                return newCounterValue;
            }
            return currentValue;
        });
        setCounterList(list);
    };
    return (
        <div>
            {counterList.map((counter,index)=>{
                return(
                    <Counter key = {index} 
                    counter = {counter} 
                    updateCounterValue ={updateCounterValue}
                    index = {index}
                    />
                    );
            })}
        </div>
    )
};
export default CounterGroup;