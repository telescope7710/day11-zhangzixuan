import { useState } from "react";

const CounterSizeGenerator=(props)=>{
    const {size, setSize} = props;

    const updateSize = (event)=>{
        setSize(event.target.value)
    };

    return(
        <div className='counterSizeGenerator'>
            size:
            <input type='number' value={size||''} onChange={updateSize} />
        </div>
    )
}
export default CounterSizeGenerator;