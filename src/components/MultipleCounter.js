import { useState } from "react";
import CounterGroup from './CounterGroup';
import CounterSizeGenerator from './CouterSizeGenerator'
import CounterGroupSum from "./CounterGroupSum";
const MultipleCounter = () => {
    const [counterList,setCounterList] = useState([]);
    

    const setSize = (size) =>{
        console.log(size);
        const list = size ? Array.from({length: size}).fill(0):[];
        setCounterList(list);
    }

    const sum = counterList.reduce((sum,currentValue) => sum+currentValue,0);
    return (
        <div>
          <CounterSizeGenerator size={counterList.length} setSize={setSize} />
          <CounterGroupSum sum = {sum} />
          <CounterGroup counterList={counterList} setCounterList={setCounterList} />
        </div>
        );
}
export default MultipleCounter;